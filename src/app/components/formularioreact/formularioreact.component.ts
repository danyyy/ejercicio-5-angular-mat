import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-formularioreact',
  templateUrl: './formularioreact.component.html',
  styleUrls: ['./formularioreact.component.css']
})
export class FormularioreactComponent implements OnInit {

  guardados:string[]=[];
  formulario!:FormGroup;
  bandera:boolean=false;
  msg!:string;
  constructor(private fb:FormBuilder) { 
    this.crearForm();
  }

  get getName(){
    return this.formulario.get('inputName');
  }

  get getNamesCheck(){
    return this.formulario.get('checkName') as FormArray;
  }


  ngOnInit(): void {
  }

  crearForm(){
    this.formulario = this.fb.group({
      inputName: ['',[Validators.required,Validators.pattern(/^[a-zA-zñÑ\s]+$/)]],
      checkName:this.fb.array([]), 
    })

   
  }

  saveALL():void{
    this.guardados=[];
    for (let i = 0; i < this.getNamesCheck.length; i++) {
      if (this.getNamesCheck.at(i).get('check')?.value ==true ) {
        console.log(this.getNamesCheck.at(i).get('check'));
        
        this.bandera=false;
        this.guardados.push( this.getNamesCheck.at(i).get('nombre')?.value);
      }
    }
    
    
    if (this.guardados.length==0) {
      this.bandera=true;
      setTimeout(() => {
        this.bandera=false;
      }, 2000);
    } 
    console.log(this.guardados);
  }



  clearCheckboxes(id:number):void{
    this.getNamesCheck.removeAt(id);
  }

  clearBox():void{
    this.guardados=[];
  }

  add():void{
    
    if (this.getName?.valid) {
      const nuevo = this.fb.group({
        check: [null],
        nombre : [this.formulario.get('inputName')?.value]
      });

      console.log(nuevo.get('name')?.value);
      
      this.getNamesCheck.push(nuevo);
    } else {
      return;
    }
  }

  toShow():string{
    let resp='';
       this.guardados.forEach((e,i)=>{
          resp += `Id: ${i} - Nombre: ${e}\n`;
      });      
    return resp;
  }

}
